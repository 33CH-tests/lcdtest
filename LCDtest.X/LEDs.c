/*! \file  LEDs.c
 *
 *  \brief Display a value in the LEDs
 *
 *
 *  \author jjmcd
 *  \date November 23, 2015, 7:53 AM
 */
/* Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>

/*! LEDs() - Display a value in the LEDs */

/*! LEDs() displays the lower three bits of the passed in parameter
 *  in the LEDs. If bit 0 is true (the number is odd), LED3, the rightmost
 *  LED is illuminated.  If bit 1, LED2, etc.
 * 
 * \param value int - Number to display in binary (3 significant bits)
 * \return none
 */
void LEDs(int value)
{
  if ( value&1 )        /* Bit 0            */
    _LATB4 = 1;         /*  true = ON       */
  else
    _LATB4 = 0;         /* false = OFF      */
  if ( value&2 )        /* Bit 1            */
    _LATB6 = 1;         /*  true = ON       */
  else
    _LATB6 = 0;         /* false = OFF      */
  if ( value&4 )        /* Bit 2            */
    _LATB7 = 1;         /*  true = ON       */
  else
    _LATB7 = 0;         /* false = OFF      */
  _LATB8 ^= 1;
}
