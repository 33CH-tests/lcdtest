/*! \file  initOscillator.c
 *
 *  \brief Initialize the oscillator
 *
 *  \author jjmcd
 *  \date July 12, 2018, 9:09 AM
 *
 * Software License Agreement
 * Copyright (c) 2018 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>

/*! initOscillator - Initialize the oscillator */
/*! Probably way overkill. Simply cut and pasted from Microchip example
 */
void initOscillator(void)
{
    /*DOZEN: Doze Mode Enable bit
    0 = Processor clock and peripheral clock ratio is forced to 1:1           */
    CLKDIVbits.DOZEN = 0;

    /* FRCDIV<2:0>: Internal Fast RC Oscillator Post-scaler bits
       000 = FRC divided by 1 (default)                                       */
    CLKDIVbits.FRCDIV = 0;

        /* VCODIV<1:0>: PLL VCO Output Divider Select bits
     * 0b11=VCO clock; 0b10=VCO/2 clock; 0b01=VCO/3 clock ; 0b00=VCO/4 clock  */
    PLLDIVbits.VCODIV = 2;

     /* In this device Internal RC Oscillator is 8MHz
     * Also,In all Motor Control Development boards primary oscillator or
     * Crystal oscillator output frequency is  8MHz
     * Hence, FPLLI (PLL Input frequency)is 8MHz in the application
     *
     * FOSC (Oscillator output frequency),FCY (Device Operating Frequency),
     * FVCO (VCO Output Frequency )is:
     *         ( FPLLI * M)     (8 * 135)
     * FVCO = -------------- = -----------  = 1080 MHz
     *               N1             1
     *               N1             1
     *
     *         (FPLLI * M)     1    (8 * 120)      1
     * FOSC = -------------- * - = -----------  * ---  = 160 MHz
     *        (N1 * N2 * N3)   2   (1 * 3 * 1)     2
     *
     * FCY  = 180 MHz / 2 =  80 MHz
     *
     * where,
     * N1 = CLKDIVbits.PLLPRE = 1
     * N2 = PLLDIVbits.POST1DIV = 3
     * N3 = PLLDIVbits.POST2DIV = 1
     * M = PLLFBDbits.PLLFBDIV = 135
     */

    /* PLL Feedback Divider bits (also denoted as ?M?, PLL multiplier)
     * M = (PLLFBDbits.PLLFBDIV)= 120                                         */
//    PLLFBDbits.PLLFBDIV = 120;
    PLLFBDbits.PLLFBDIV = 80;

    /* PLL Phase Detector I/P Divider Select bits(denoted as ?N1?,PLL pre-scaler)
     * N1 = CLKDIVbits.PLLPRE = 1                                             */
    CLKDIVbits.PLLPRE = 1;

    /* PLL Output Divider #1 Ratio bits((denoted as 'N2' or POSTDIV#1)
     * N2 = PLLDIVbits.POST1DIV = 3                                           */
    PLLDIVbits.POST1DIV = 3;

    /* PLL Output Divider #2 Ratio bits((denoted as 'N3' or POSTDIV#2)
     * N3 = PLLDIVbits.POST2DIV = 1                                           */
    PLLDIVbits.POST2DIV = 1;

    /* Initiate Clock Switch to FRC Oscillator with PLL (NOSC=0b001)
     *  NOSC = 0b001 = Fast RC Oscillator with PLL (FRCPLL)                   */
    __builtin_write_OSCCONH(0x01);

    /* Request oscillator switch to selection specified by the NOSC<2:0>bits  */
    __builtin_write_OSCCONL(OSCCON || 0x01);

    /* Wait for Clock switch to occur */
    while (OSCCONbits.COSC != 0b001);

    /* Wait for PLL to lock */
    while (OSCCONbits.LOCK != 1);
}
