/*! \file  snore.c
 *
 *  \brief
 *
 *  \author jjmcd
 *  \date July 27, 2018, 2:19 PM
 *
 * Software License Agreement
 * Copyright (c) 2018 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */


/*! snore - */

/*!
 *
 */
/*! snore - waste some time */
void snore(unsigned long j)
{
  long i;
  for (i = 0; i < j; i++)
    ;
}

