#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-default.mk)" "nbproject/Makefile-local-default.mk"
include nbproject/Makefile-local-default.mk
endif
endif

# Environment
MKDIR=mkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=default
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=elf
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/LCDtest.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/LCDtest.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

ifeq ($(COMPARE_BUILD), true)
COMPARISON_BUILD=-mafrlcsj
else
COMPARISON_BUILD=
endif

ifdef SUB_IMAGE_ADDRESS
SUB_IMAGE_ADDRESS_COMMAND=--image-address $(SUB_IMAGE_ADDRESS)
else
SUB_IMAGE_ADDRESS_COMMAND=
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=snore.c initOscillator.c configurationBits.c LCDtest.c linearizePosition.c compDate.c LEDs.c /home/jjmcd/Projects/PIC/dsPIC/33CH/dsPIC-EL-CH/LCDtest/LCDtestS1.X/dist/default/${IMAGE_TYPE}/LCDtestS1.s 

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/snore.o ${OBJECTDIR}/initOscillator.o ${OBJECTDIR}/configurationBits.o ${OBJECTDIR}/LCDtest.o ${OBJECTDIR}/linearizePosition.o ${OBJECTDIR}/compDate.o ${OBJECTDIR}/LEDs.o ${OBJECTDIR}/LCDtestS1.o 
POSSIBLE_DEPFILES=${OBJECTDIR}/snore.o.d ${OBJECTDIR}/initOscillator.o.d ${OBJECTDIR}/configurationBits.o.d ${OBJECTDIR}/LCDtest.o.d ${OBJECTDIR}/linearizePosition.o.d ${OBJECTDIR}/compDate.o.d ${OBJECTDIR}/LEDs.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/snore.o ${OBJECTDIR}/initOscillator.o ${OBJECTDIR}/configurationBits.o ${OBJECTDIR}/LCDtest.o ${OBJECTDIR}/linearizePosition.o ${OBJECTDIR}/compDate.o ${OBJECTDIR}/LEDs.o ${OBJECTDIR}/LCDtestS1.o 

# Source Files
SOURCEFILES=snore.c initOscillator.c configurationBits.c LCDtest.c linearizePosition.c compDate.c LEDs.c /home/jjmcd/Projects/PIC/dsPIC/33CH/dsPIC-EL-CH/LCDtest/LCDtestS1.X/dist/default/${IMAGE_TYPE}/LCDtestS1.s 


CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-default.mk dist/${CND_CONF}/${IMAGE_TYPE}/LCDtest.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=33CH128MP506
MP_LINKER_FILE_OPTION=,--script=p33CH128MP506.gld
# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/snore.o: snore.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/snore.o.d 
	@${RM} ${OBJECTDIR}/snore.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  snore.c  -o ${OBJECTDIR}/snore.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/snore.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)   -I"/home/jjmcd/Projects/PIC/dsPIC/33CH/dsPIC-EL-CH/LCDtest/LCDtestS1.X/dist/default/production"  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/snore.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/initOscillator.o: initOscillator.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/initOscillator.o.d 
	@${RM} ${OBJECTDIR}/initOscillator.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  initOscillator.c  -o ${OBJECTDIR}/initOscillator.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/initOscillator.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)   -I"/home/jjmcd/Projects/PIC/dsPIC/33CH/dsPIC-EL-CH/LCDtest/LCDtestS1.X/dist/default/production"  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/initOscillator.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/configurationBits.o: configurationBits.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/configurationBits.o.d 
	@${RM} ${OBJECTDIR}/configurationBits.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  configurationBits.c  -o ${OBJECTDIR}/configurationBits.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/configurationBits.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)   -I"/home/jjmcd/Projects/PIC/dsPIC/33CH/dsPIC-EL-CH/LCDtest/LCDtestS1.X/dist/default/production"  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/configurationBits.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/LCDtest.o: LCDtest.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/LCDtest.o.d 
	@${RM} ${OBJECTDIR}/LCDtest.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  LCDtest.c  -o ${OBJECTDIR}/LCDtest.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/LCDtest.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)   -I"/home/jjmcd/Projects/PIC/dsPIC/33CH/dsPIC-EL-CH/LCDtest/LCDtestS1.X/dist/default/production"  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/LCDtest.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/linearizePosition.o: linearizePosition.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/linearizePosition.o.d 
	@${RM} ${OBJECTDIR}/linearizePosition.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  linearizePosition.c  -o ${OBJECTDIR}/linearizePosition.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/linearizePosition.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)   -I"/home/jjmcd/Projects/PIC/dsPIC/33CH/dsPIC-EL-CH/LCDtest/LCDtestS1.X/dist/default/production"  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/linearizePosition.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/compDate.o: compDate.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/compDate.o.d 
	@${RM} ${OBJECTDIR}/compDate.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  compDate.c  -o ${OBJECTDIR}/compDate.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/compDate.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)   -I"/home/jjmcd/Projects/PIC/dsPIC/33CH/dsPIC-EL-CH/LCDtest/LCDtestS1.X/dist/default/production"  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/compDate.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/LEDs.o: LEDs.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/LEDs.o.d 
	@${RM} ${OBJECTDIR}/LEDs.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  LEDs.c  -o ${OBJECTDIR}/LEDs.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/LEDs.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)   -I"/home/jjmcd/Projects/PIC/dsPIC/33CH/dsPIC-EL-CH/LCDtest/LCDtestS1.X/dist/default/production"  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/LEDs.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
else
${OBJECTDIR}/snore.o: snore.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/snore.o.d 
	@${RM} ${OBJECTDIR}/snore.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  snore.c  -o ${OBJECTDIR}/snore.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/snore.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)   -I"/home/jjmcd/Projects/PIC/dsPIC/33CH/dsPIC-EL-CH/LCDtest/LCDtestS1.X/dist/default/production"  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/snore.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/initOscillator.o: initOscillator.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/initOscillator.o.d 
	@${RM} ${OBJECTDIR}/initOscillator.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  initOscillator.c  -o ${OBJECTDIR}/initOscillator.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/initOscillator.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)   -I"/home/jjmcd/Projects/PIC/dsPIC/33CH/dsPIC-EL-CH/LCDtest/LCDtestS1.X/dist/default/production"  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/initOscillator.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/configurationBits.o: configurationBits.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/configurationBits.o.d 
	@${RM} ${OBJECTDIR}/configurationBits.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  configurationBits.c  -o ${OBJECTDIR}/configurationBits.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/configurationBits.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)   -I"/home/jjmcd/Projects/PIC/dsPIC/33CH/dsPIC-EL-CH/LCDtest/LCDtestS1.X/dist/default/production"  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/configurationBits.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/LCDtest.o: LCDtest.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/LCDtest.o.d 
	@${RM} ${OBJECTDIR}/LCDtest.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  LCDtest.c  -o ${OBJECTDIR}/LCDtest.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/LCDtest.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)   -I"/home/jjmcd/Projects/PIC/dsPIC/33CH/dsPIC-EL-CH/LCDtest/LCDtestS1.X/dist/default/production"  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/LCDtest.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/linearizePosition.o: linearizePosition.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/linearizePosition.o.d 
	@${RM} ${OBJECTDIR}/linearizePosition.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  linearizePosition.c  -o ${OBJECTDIR}/linearizePosition.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/linearizePosition.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)   -I"/home/jjmcd/Projects/PIC/dsPIC/33CH/dsPIC-EL-CH/LCDtest/LCDtestS1.X/dist/default/production"  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/linearizePosition.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/compDate.o: compDate.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/compDate.o.d 
	@${RM} ${OBJECTDIR}/compDate.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  compDate.c  -o ${OBJECTDIR}/compDate.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/compDate.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)   -I"/home/jjmcd/Projects/PIC/dsPIC/33CH/dsPIC-EL-CH/LCDtest/LCDtestS1.X/dist/default/production"  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/compDate.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/LEDs.o: LEDs.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/LEDs.o.d 
	@${RM} ${OBJECTDIR}/LEDs.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  LEDs.c  -o ${OBJECTDIR}/LEDs.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/LEDs.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)   -I"/home/jjmcd/Projects/PIC/dsPIC/33CH/dsPIC-EL-CH/LCDtest/LCDtestS1.X/dist/default/production"  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/LEDs.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assemblePreproc
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble_subordinate
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/LCDtestS1.o: /home/jjmcd/Projects/PIC/dsPIC/33CH/dsPIC-EL-CH/LCDtest/LCDtestS1.X/dist/default/${IMAGE_TYPE}/LCDtestS1.s  nbproject/Makefile-${CND_CONF}.mk
	${MP_CC} -c -mcpu=$(MP_PROCESSOR_OPTION)  /home/jjmcd/Projects/PIC/dsPIC/33CH/dsPIC-EL-CH/LCDtest/LCDtestS1.X/dist/default/${IMAGE_TYPE}/LCDtestS1.s  -o ${OBJECTDIR}/LCDtestS1.o
else
${OBJECTDIR}/LCDtestS1.o: /home/jjmcd/Projects/PIC/dsPIC/33CH/dsPIC-EL-CH/LCDtest/LCDtestS1.X/dist/default/${IMAGE_TYPE}/LCDtestS1.s  nbproject/Makefile-${CND_CONF}.mk
	${MP_CC} -c -mcpu=$(MP_PROCESSOR_OPTION)  /home/jjmcd/Projects/PIC/dsPIC/33CH/dsPIC-EL-CH/LCDtest/LCDtestS1.X/dist/default/${IMAGE_TYPE}/LCDtestS1.s  -o ${OBJECTDIR}/LCDtestS1.o
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/LCDtest.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk  ../../LCD/LCDlib.X/dist/default/debug/LCDlib.X.a  
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -o dist/${CND_CONF}/${IMAGE_TYPE}/LCDtest.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}    ../../LCD/LCDlib.X/dist/default/debug/LCDlib.X.a  -mcpu=$(MP_PROCESSOR_OPTION)        -D__DEBUG=__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)   -I"/home/jjmcd/Projects/PIC/dsPIC/33CH/dsPIC-EL-CH/LCDtest/LCDtestS1.X/dist/default/production"   -mreserve=data@0x1000:0x101B -mreserve=data@0x101C:0x101D -mreserve=data@0x101E:0x101F -mreserve=data@0x1020:0x1021 -mreserve=data@0x1022:0x1023 -mreserve=data@0x1024:0x1027 -mreserve=data@0x1028:0x104F   -Wl,--local-stack,,--defsym=__MPLAB_BUILD=1,--defsym=__MPLAB_DEBUG=1,--defsym=__DEBUG=1,-D__DEBUG=__DEBUG,--defsym=__MPLAB_DEBUGGER_ICD3=1,$(MP_LINKER_FILE_OPTION),--stack=16,--check-sections,--data-init,--pack-data,--handles,--isr,--no-gc-sections,--fill-upper=0,--stackguard=16,--no-force-link,--smart-io,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map",--report-mem,--memorysummary,dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml$(MP_EXTRA_LD_POST) 
	
else
dist/${CND_CONF}/${IMAGE_TYPE}/LCDtest.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk  ../../LCD/LCDlib.X/dist/default/production/LCDlib.X.a 
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -o dist/${CND_CONF}/${IMAGE_TYPE}/LCDtest.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}    ../../LCD/LCDlib.X/dist/default/production/LCDlib.X.a  -mcpu=$(MP_PROCESSOR_OPTION)        -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)   -I"/home/jjmcd/Projects/PIC/dsPIC/33CH/dsPIC-EL-CH/LCDtest/LCDtestS1.X/dist/default/production"  -Wl,--local-stack,,--defsym=__MPLAB_BUILD=1,$(MP_LINKER_FILE_OPTION),--stack=16,--check-sections,--data-init,--pack-data,--handles,--isr,--no-gc-sections,--fill-upper=0,--stackguard=16,--no-force-link,--smart-io,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map",--report-mem,--memorysummary,dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml$(MP_EXTRA_LD_POST) 
	${MP_CC_DIR}/xc16-bin2hex dist/${CND_CONF}/${IMAGE_TYPE}/LCDtest.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} -a  -omf=elf  
	
endif


# Subprojects
.build-subprojects:
	cd ../../LCD/LCDlib.X && ${MAKE}  -f Makefile CONF=default
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
	cd ../LCDtestS1.X && ${MAKE}  -f Makefile CONF=default TYPE_IMAGE=DEBUG_RUN
	cd ../LCDtestS1.X && ${MAKE}  -f Makefile CONF=default TYPE_IMAGE=DEBUG_RUN SUB_IMAGE_NAME=LCDtestS1 .build-sub-images-impl
else
	cd ../LCDtestS1.X && ${MAKE}  -f Makefile CONF=default
	cd ../LCDtestS1.X && ${MAKE}  -f Makefile CONF=default SUB_IMAGE_NAME=LCDtestS1 .build-sub-images-impl
endif


# Subprojects
.clean-subprojects:
	cd ../../LCD/LCDlib.X && rm -rf "build/default" "dist/default"
	cd ../LCDtestS1.X && rm -rf "build/default" "dist/default"

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/default
	${RM} -r dist/default

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell "${PATH_TO_IDE_BIN}"mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
