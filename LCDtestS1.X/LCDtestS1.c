/*! \file  LCDtestS1.c
 *
 *  \brief This file contains the mainline for LCDtestS1
 *
 *
 *  \author jjmcd
 *  \date December 24, 2018, 10:32 AM
 *
 * Software License Agreement
 * Copyright (c) 2018 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */

#include <xc.h>

#define ON_LENGTH   250000
#define OFF_LENGTH 1750000

void snore( unsigned long l)
{
  unsigned long i;
  for ( i=0; i<l; i++ )
    ;
}

void blinkit( int nLED )
{
  switch (nLED)
    {
      case 0:
        _LATC12=1;
        snore(ON_LENGTH);
        _LATC12 = 0;
        snore(OFF_LENGTH);
        break;
      case 1:
        _LATC13=1;
        snore(ON_LENGTH);
        _LATC13 = 0;
        snore(OFF_LENGTH);
        break;
      case 2:
        _LATC14=1;
        snore(ON_LENGTH);
        _LATC14 = 0;
        snore(OFF_LENGTH);
        break;
      case 3:
        _LATC15=1;
        snore(ON_LENGTH);
        _LATC15 = 0;
        snore(OFF_LENGTH);
        break;
      case 4:
        _LATD15=1;
        snore(ON_LENGTH);
        _LATD15 = 0;
        snore(OFF_LENGTH);
        break;
      case 5:
        _LATD14=1;
        snore(ON_LENGTH);
        _LATD14 = 0;
        snore(OFF_LENGTH);
        break;
    }
}


/*! main - Mainline for LCDtestS1 */

/*!
 *
 */
int main(void)
{
  int i;

  TRISC=0;
  TRISD=0;

  while (1)
    {
      for ( i=0; i<6; i++ )
        blinkit(i);
    }

  return 0;
}
